variable "region" {
  description = "AWS region to deploy EKS cluster."
  type        = string
}

variable "instance_type" {
  description = "Instance type for EKS worker nodes."
  type        = string
}

variable "node_desired_capacity" {
  description = "Desired number of worker nodes."
  type        = number
}

variable "node_max_capacity" {
  description = "Maximum number of worker nodes."
  type        = number
}

variable "node_min_capacity" {
  description = "Minimum number of worker nodes."
  type        = number
}

variable "env" {
  type        = string
  description = "The environment name e.g prod/staging/dev"
}





