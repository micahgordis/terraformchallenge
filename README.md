# Kubernetes Nginx Deployment on AWS

## Overview

This repository contains a project that leverages Terraform and Helm to deploy a Kubernetes cluster in AWS, complete with an Nginx pod accessible via the internet. The Terraform configuration sets up the necessary AWS infrastructure, including a VPC, subnets, and other resources required for a functional Kubernetes cluster. Helm is then used to deploy the Nginx pod, making it accessible through an AWS Load Balancer.

## Prerequisites

Before you begin, ensure you have the following software installed on your system:

- AWS CLI: `aws-cli/2.15.21`
- kubectl: `Client Version: v1.29.2`, `Server Version: v1.29.0-eks-c417bb3`
- Helm: `Version:"v3.14.2"`
- Terraform: `v1.5.7`

Additionally, you must configure your AWS credentials to allow Terraform to manage resources on your behalf:

### Configuring AWS Credentials

1. **AWS CLI Method**: If you have the AWS CLI installed, you can run `aws configure` and follow the prompts to input your `AWS Access Key ID`, `AWS Secret Access Key`, and default region.

2. **Environment Variables**: Alternatively, you can set the following environment variables:
   - `AWS_ACCESS_KEY_ID`: Your AWS Access Key ID.
   - `AWS_SECRET_ACCESS_KEY`: Your AWS Secret Access Key.
   - `AWS_DEFAULT_REGION`: Your default AWS region (e.g., `us-west-2`).

   These can be set in your terminal session or added to your profile script (e.g., `.bash_profile`, `.zshrc`) for persistence.

Ensure your AWS user has sufficient permissions to create and manage the AWS resources defined in the Terraform configuration.

## Team Collaboration and Remote Backend

If you plan on working on this project as a team, it's highly recommended to set up a remote backend for Terraform, such as Amazon S3. A remote backend allows your team to share the Terraform state and lock the state to prevent conflicts. For instructions on setting up an S3 backend, refer to the [Terraform documentation](https://www.terraform.io/docs/language/settings/backends/s3.html).

## Usage

### Deploy the Project

1. Clone the repository and navigate to the directory:

git clone <repository-url>
cd <repository-directory>


2. Run the script with the `deploy` command followed by the environment (`staging` or `production`):

./manage-project.sh deploy staging

This command deploys the AWS infrastructure with Terraform and the nginx deployment using Helm.

./manage-project.sh destroy staging

This command removes the Helm deployment and destroys the AWS infrastructure created by Terraform.

## Customization

You can customize the deployment by modifying the `values*.yaml` files in the `nginx-chart` directory for Helm configurations and the `.tfvars` files in the `environments` directory for Terraform configurations.

## Cleanup and Costs

Remember to destroy the resources when they are no longer needed to avoid incurring ongoing costs for AWS resources. The `destroy` command in the usage section outlines how to cleanly remove all resources associated with the deployment.
