#!/bin/bash

# Function to check if a command exists
command_exists() {
    command -v "$1" >/dev/null 2>&1
}

# Check for required tools
REQUIRED_TOOLS=("helm" "kubectl" "aws" "terraform")
for tool in "${REQUIRED_TOOLS[@]}"; do
    if ! command_exists "$tool"; then
        echo "Error: Required tool '$tool' is not installed."
        exit 1
    fi
done

# Verify correct number of arguments
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <deploy|destroy> <environment>"
    echo "Example: $0 deploy staging"
    exit 1
fi

COMMAND=$1
ENVIRONMENT=$2

# Validate command
if [ "$COMMAND" != "deploy" ] && [ "$COMMAND" != "destroy" ]; then
    echo "Error: Command must be 'deploy' or 'destroy'."
    exit 1
fi

# Validate environment
if [ "$ENVIRONMENT" != "staging" ] && [ "$ENVIRONMENT" != "production" ]; then
    echo "Error: Environment must be 'staging' or 'production'."
    exit 1
fi

# Define EKS cluster name based on environment
EKS_CLUSTER_NAME="${ENVIRONMENT}-cluster"

# Extract the AWS region from the tfvars file, ensuring only the first match is considered and trimming CR/LF characters
AWS_REGION=$(grep 'region' "environments/$ENVIRONMENT.tfvars" | head -n 1 | awk -F '= "' '{print $2}' | sed 's/"//g' | tr -d '\n' | tr -d '\r')

if [ -z "$AWS_REGION" ]; then
    echo "Error: Unable to extract AWS region from environments/$ENVIRONMENT.tfvars."
    exit 1
fi



# Function to deploy infrastructure and application
deploy() {
    # Terraform initialization and workspace selection
    terraform init
    terraform workspace select "$ENVIRONMENT" || terraform workspace new "$ENVIRONMENT"
    
    # Terraform deployment
    terraform apply -auto-approve -var-file="environments/$ENVIRONMENT.tfvars"

    # Use the extracted AWS region in the aws eks update-kubeconfig command
    echo "Updating kubeconfig for $EKS_CLUSTER_NAME in region $AWS_REGION..."
    aws eks update-kubeconfig --name "$EKS_CLUSTER_NAME" --region "$AWS_REGION"

    # Helm deployment
    helm upgrade --install nginx-release ./nginx-chart -f "nginx-chart/values.$ENVIRONMENT.yaml"
    
    # Retrieve and monitor the Load Balancer DNS
    echo "Waiting for Load Balancer DNS..."
    if date --version >/dev/null 2>&1; then
        # GNU date (Linux)
        END_TIME=$(date -d "+10 minutes" +%s)
    else
        # BSD date (macOS)
        END_TIME=$(date -v+10M +%s)
    fi
    while true; do
        LOAD_BALANCER_DNS=$(kubectl get service nginx-service -n default -o jsonpath='{.status.loadBalancer.ingress[0].hostname}')
        if [ -n "$LOAD_BALANCER_DNS" ]; then
            echo "Load Balancer DNS: $LOAD_BALANCER_DNS"
            break
        elif [ "$(date +%s)" -gt "$END_TIME" ]; then
            echo "Timeout reached: Load Balancer DNS not available after 10 minutes. There might be an issue."
            exit 1
        fi
        echo "Still waiting for Load Balancer DNS..."
        sleep 10
    done
}

# Function to destroy infrastructure and application
destroy() {
    # Terraform initialization and workspace selection
    terraform init
    terraform workspace select "$ENVIRONMENT" || terraform workspace new "$ENVIRONMENT"

    # Use the extracted AWS region in the aws eks update-kubeconfig command
    echo "Updating kubeconfig for $EKS_CLUSTER_NAME in region $AWS_REGION..."
    aws eks update-kubeconfig --name "$EKS_CLUSTER_NAME" --region "$AWS_REGION"

    # Check if the Helm release exists
    if helm list -n default --filter "^nginx-release$" | grep -q "nginx-release"; then
        echo "Helm release nginx-release found. Proceeding with uninstall..."
        helm uninstall nginx-release -n default
    else
        echo "Helm release nginx-release not found. Skipping uninstall."
    fi
    
    # Terraform destruction
    echo "Destroying Terraform-managed infrastructure..."
    terraform destroy -auto-approve -var-file="environments/$ENVIRONMENT.tfvars"
}

# Execute based on command
case $COMMAND in
    deploy)
        deploy
        ;;
    destroy)
        destroy
        ;;
    *)
        echo "Invalid command. Use 'deploy' or 'destroy'."
        exit 1
        ;;
esac
