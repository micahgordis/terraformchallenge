region                  = "us-west-2"
env                     = "production"
instance_type           = "m5.large"
node_desired_capacity   = 1
node_max_capacity       = 3
node_min_capacity       = 0
