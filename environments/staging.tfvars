region                  = "us-west-2"
env                     = "staging"
instance_type           = "m5.large"
node_desired_capacity   = 1
node_max_capacity       = 2
node_min_capacity       = 0
