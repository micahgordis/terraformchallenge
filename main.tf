provider "aws" {
  region = var.region
}

locals {
  cluster_name       = "${var.env}-cluster"
  cluster_version    = "1.29"
  node_group_name    = "${var.env}-node-group"
  vpc_cidr           = "10.0.0.0/16"
  availability_zones = ["us-west-2a", "us-west-2b"]
  public_subnets     = ["10.0.1.0/24", "10.0.2.0/24"]
  private_subnets    = ["10.0.4.0/24", "10.0.5.0/24"]
}

module "vpc" {
  source             = "terraform-aws-modules/vpc/aws"
  version            = "5.5.2"
  name               = local.cluster_name
  cidr               = local.vpc_cidr
  azs                = local.availability_zones
  private_subnets    = local.private_subnets
  public_subnets     = local.public_subnets
  enable_nat_gateway = true
  enable_vpn_gateway = true
}

module "eks" {
  source                                   = "terraform-aws-modules/eks/aws"
  cluster_name                             = local.cluster_name
  cluster_version                          = local.cluster_version
  subnet_ids                               = module.vpc.public_subnets
  vpc_id                                   = module.vpc.vpc_id
  cluster_endpoint_public_access           = true
  enable_cluster_creator_admin_permissions = true


  self_managed_node_groups = {
    "${local.node_group_name}" = {
      name                       = local.node_group_name
      instance_type              = var.instance_type
      desired_size               = var.node_desired_capacity
      max_size                   = var.node_max_capacity
      min_size                   = var.node_min_capacity
      subnet_ids                 = module.vpc.private_subnets
      enable_bootstrap_user_data = true
      ami_id                     = data.aws_ami.eks_default.id

      create_iam_role          = true
      iam_role_name            = "${var.env}-self-managed-node-group"
      iam_role_use_name_prefix = false
      iam_role_description     = "Self managed node group for ${var.env} environment"
      iam_role_additional_policies = {
        AmazonEC2ContainerRegistryReadOnly = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
      }
    }
  }
}

data "aws_ami" "eks_default" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amazon-eks-node-${local.cluster_version}-v*"]
  }
}


